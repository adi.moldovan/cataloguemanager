﻿namespace Models
{
    public class FactoryProduct
    {
        public int ProductId { get; set; }

        public Product Product { get; set; }

        public int FactoryId { get; set; }

        public Factory Factory { get; set; }

        public int Cost { get; set; }

        public int Capacity { get; set; }
    }
}