﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Order
    {
        public int Id { get; set; }

        [Required]
        public string CustomerName { get; set; }
        
        public DateTime Deadline { get; set; }

        public List<OrderProduct> Products { get; set; }
    }
}