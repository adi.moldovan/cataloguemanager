﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Models
{
    public class Factory
    {
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }

        public List<FactoryProduct> Products { get; set; }
    }
}