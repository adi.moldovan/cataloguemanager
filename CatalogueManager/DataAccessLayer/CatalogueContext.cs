﻿using Microsoft.EntityFrameworkCore;
using Models;

namespace DataAccessLayer
{
    public class CatalogueContext : DbContext
    {
        public CatalogueContext(DbContextOptions<CatalogueContext> options) : base(options) { }

        public DbSet<Product> Products { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderProduct> OrderProducts { get; set; }

        public DbSet<Factory> Factories { get; set; }

        public DbSet<FactoryProduct> FactoryProducts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderProduct>().HasKey(op => new { op.OrderId, op.ProductId });
            modelBuilder.Entity<FactoryProduct>().HasKey(fp => new { fp.FactoryId, fp.ProductId });
        }
    }
}