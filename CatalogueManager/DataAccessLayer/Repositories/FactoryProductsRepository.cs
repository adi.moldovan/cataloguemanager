﻿using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class FactoryProductsRepository : IFactoryProductsRepository
    {
        private CatalogueContext _context { get; }

        public FactoryProductsRepository(CatalogueContext context)
        {
            _context = context;
        }

        public Task<List<FactoryProduct>> GetAllFactoryProducts()
        {
            return _context.FactoryProducts
                .Include(op => op.Factory)
                .Include(op => op.Product)
                .ToListAsync();
        }
    }
}