﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IFactoryProductsRepository
    {
        public Task<List<FactoryProduct>> GetAllFactoryProducts();
    }
}