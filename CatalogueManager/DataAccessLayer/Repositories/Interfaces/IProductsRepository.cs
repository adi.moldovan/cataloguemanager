﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IProductsRepository
    {
        Task AddProductAsync(Product product);

        Task<List<Product>> GetAllProductsAsync();
    }
}