﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IFactoriesRepository
    {
        Task AddFactoryAsync(Factory factory);

        Task<List<Factory>> GetAllFactoriesAsnyc();
    }
}