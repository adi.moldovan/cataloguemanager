﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderProductsRepository
    {
        public Task<List<OrderProduct>> GetAllOrderProducts();
    }
}