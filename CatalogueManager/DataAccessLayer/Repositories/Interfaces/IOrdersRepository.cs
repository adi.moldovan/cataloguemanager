﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories.Interfaces
{
    public interface IOrdersRepository
    {
        Task AddOrderAsync(Order order);

        Task<List<Order>> GetAllOrdersAsnyc();

        Task<List<Order>> GetAllActiveOrdersAsnyc();
    }
}