﻿using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class OrdersRepository : IOrdersRepository
    {
        private CatalogueContext _context { get; }

        public OrdersRepository(CatalogueContext context)
        {
            _context = context;
        }

        public async Task AddOrderAsync(Order order)
        {
            _context.Orders.Add(order);

            await _context.SaveChangesAsync();
        }

        public Task<List<Order>> GetAllOrdersAsnyc()
        {
            return _context
                .Orders
                .Include(o => o.Products).ThenInclude(p => p.Product)
                .ToListAsync();
        }

        public Task<List<Order>> GetAllActiveOrdersAsnyc()
        {
            return _context
                .Orders
                .Include(o => o.Products.Where(p => p.Product.Factories.Any())).ThenInclude(p => p.Product).ThenInclude(p => p.Factories)
                .Where(o => o.Deadline >= DateTime.Now && o.Products.Any(p => p.Product.Factories.Any()))
                .ToListAsync();
        }
    }
}