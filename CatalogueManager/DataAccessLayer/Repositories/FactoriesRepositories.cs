﻿using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class FactoriesRepositories : IFactoriesRepository
    {
        private CatalogueContext _context { get; }

        public FactoriesRepositories(CatalogueContext context)
        {
            _context = context;
        }

        public async Task AddFactoryAsync(Factory factory)
        {
            _context.Factories.Add(factory);

            await _context.SaveChangesAsync();
        }

        public Task<List<Factory>> GetAllFactoriesAsnyc()
        {
            return _context
                .Factories
                .Include(o => o.Products).ThenInclude(p => p.Product)
                .ToListAsync();
        }
    }
}