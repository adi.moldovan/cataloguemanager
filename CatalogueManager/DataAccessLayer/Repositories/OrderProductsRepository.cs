﻿using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class OrderProductsRepository : IOrderProductsRepository
    {
        private CatalogueContext _context { get; }

        public OrderProductsRepository(CatalogueContext context)
        {
            _context = context;
        }

        public Task<List<OrderProduct>> GetAllOrderProducts()
        {
            return _context.OrderProducts
                .Include(op => op.Order)
                .Include(op => op.Product)
                .ToListAsync();
        }
    }
}