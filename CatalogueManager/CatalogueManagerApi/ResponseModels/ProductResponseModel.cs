﻿namespace CatalogueManagerApi.ResponseModels
{
    public class ProductResponseModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Price { get; set; }
    }
}