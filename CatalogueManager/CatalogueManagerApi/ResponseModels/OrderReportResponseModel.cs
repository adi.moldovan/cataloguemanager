﻿using System;
using System.Collections.Generic;

namespace CatalogueManagerApi.ResponseModels
{
    public class OrderReportResponseModel
    {
        public int Id { get; set; }

        public string CustomerName { get; set; }

        public DateTime Deadline { get; set; }

        public List<OrderProductResponseModel> Items { get; set; }

        public int OveralCost { get; set; }
    }
}