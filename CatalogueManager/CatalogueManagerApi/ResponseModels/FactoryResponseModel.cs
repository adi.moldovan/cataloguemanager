﻿using System.Collections.Generic;

namespace CatalogueManagerApi.ResponseModels
{
    public class FactoryResponseModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<FactoryProductResponseModel> Capabilities { get; set; }
    }
}
