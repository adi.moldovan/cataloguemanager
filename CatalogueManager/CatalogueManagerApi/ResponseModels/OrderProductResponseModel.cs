﻿namespace CatalogueManagerApi.ResponseModels
{
    public class OrderProductResponseModel
    {
        public ProductResponseModel Product { get; set; }

        public int Quantity { get; set; }
    }
}