﻿namespace CatalogueManagerApi.ResponseModels
{
    public class OrderProductWithOrderDetailsResponseModel
    {
        public OrderSummaryResponseModel Order { get; set; }

        public ProductResponseModel Product { get; set; }

        public int Quantity { get; set; }
    }
}