﻿namespace CatalogueManagerApi.ResponseModels
{
    public class FactoryProductWithFactoryDetailsResponseModel
    {
        public FactorySummaryResponseModel Factory { get; set; }

        public ProductResponseModel Product { get; set; }

        public int Cost { get; set; }

        public int Capacity { get; set; }
    }
}