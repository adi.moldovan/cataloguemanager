﻿namespace CatalogueManagerApi.ResponseModels
{
    public class FactoryProductResponseModel
    {
        public ProductResponseModel Product { get; set; }

        public int Cost { get; set; }

        public int Capacity { get; set; }
    }
}