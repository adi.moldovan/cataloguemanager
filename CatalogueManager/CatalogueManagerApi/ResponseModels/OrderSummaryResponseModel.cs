﻿using System;

namespace CatalogueManagerApi.ResponseModels
{
    public class OrderSummaryResponseModel
    {
        public int Id { get; set; }

        public string CustomerName { get; set; }

        public DateTime Deadline { get; set; }
    }
}