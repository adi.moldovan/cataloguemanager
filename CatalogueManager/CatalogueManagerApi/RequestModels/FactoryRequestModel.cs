﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CatalogueManagerApi.RequestModels
{
    public class FactoryRequestModel
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public List<FactoryProductRequestModel> Capabilities { get; set; }
    }
}