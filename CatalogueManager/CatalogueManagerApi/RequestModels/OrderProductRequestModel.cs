﻿using System.ComponentModel.DataAnnotations;

namespace CatalogueManagerApi.RequestModels
{
    public class OrderProductRequestModel
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        public int Quantity { get; set; }
    }
}