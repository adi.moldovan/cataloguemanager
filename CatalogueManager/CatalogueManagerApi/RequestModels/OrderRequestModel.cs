﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CatalogueManagerApi.RequestModels
{
    public class OrderRequestModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string CustomerName { get; set; }

        [Required]
        public DateTime Deadline { get; set; }

        [MinLength(1)]
        public List<OrderProductRequestModel> Items { get; set; }
    }
}