﻿using System.ComponentModel.DataAnnotations;

namespace CatalogueManagerApi.RequestModels
{
    public class FactoryProductRequestModel
    {
        [Required]
        public int ProductId { get; set; }

        [Required]
        public int Cost { get; set; }

        [Required]
        public int Capacity { get; set; }
    }
}