﻿using System.ComponentModel.DataAnnotations;

namespace CatalogueManagerApi.RequestModels
{
    public class ProductRequestModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public int Price { get; set; }
    }
}