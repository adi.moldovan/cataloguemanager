﻿using BL.UnitOfWorks;
using DataAccessLayer.Repositories;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CatalogueManagerApi.Configuration
{
    public static class UnitOfWorkConfiguration
    {
        public static IServiceCollection ConfigureUnitOfWork(this IServiceCollection services)
        {
            services.AddScoped<IProductsRepository, ProductsRepository>();
            services.AddScoped<IOrdersRepository, OrdersRepository>();
            services.AddScoped<IOrderProductsRepository, OrderProductsRepository>();
            services.AddScoped<IFactoriesRepository, FactoriesRepositories>();
            services.AddScoped<IFactoryProductsRepository, FactoryProductsRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}