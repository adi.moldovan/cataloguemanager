﻿using BL.Managers;
using BL.Managers.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace CatalogueManagerApi.Configuration
{
    public static class MangersConfiguration
    {
        public static IServiceCollection ConfigureManagers(this IServiceCollection services)
        {
            services.AddScoped<IProductsManager, ProductsManager>();
            services.AddScoped<IOrdersManager, OrdersManager>();
            services.AddScoped<IFactoriesManager, FactoriesManager>();

            return services;
        }
    }
}