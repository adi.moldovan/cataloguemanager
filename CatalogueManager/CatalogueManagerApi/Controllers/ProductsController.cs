﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using BL.Managers.Interfaces;
using CatalogueManagerApi.RequestModels;
using CatalogueManagerApi.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace CatalogueManagerApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductsManager _productsManger;

        public ProductsController(IMapper mapper, IProductsManager productsManger)
        {
            _mapper = mapper;
            _productsManger = productsManger;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProductsAsnyc()
        {
            var allProducts = await _productsManger.GetAllProductsAsync();

            return Ok(_mapper.Map<List<ProductResponseModel>>(allProducts));
        }

        [HttpPost]
        [Route("addProduct")]
        public async Task<IActionResult> AddProductAsync(ProductRequestModel model)
        {
            await _productsManger.AddProductAsync(_mapper.Map<Product>(model));

            return Ok();
        }
    }
}