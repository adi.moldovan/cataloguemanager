﻿using AutoMapper;
using BL.Managers.Interfaces;
using CatalogueManagerApi.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CatalogueManagerApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ReportsController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IOrdersManager _ordersManager;

        public ReportsController(IMapper mapper, IOrdersManager ordersManager)
        {
            _mapper = mapper;
            _ordersManager = ordersManager;
        }

        [HttpGet]
        [Route("orders")]
        public async Task<IActionResult> GetAllOrdersAsnyc()
        {
            var orders = await _ordersManager.GetAllActiveOrdersAsync();

            return Ok(_mapper.Map<List<OrderReportResponseModel>>(orders));
        }
    }
}