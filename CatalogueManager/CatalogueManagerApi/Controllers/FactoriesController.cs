﻿using AutoMapper;
using BL.Managers.Interfaces;
using CatalogueManagerApi.RequestModels;
using CatalogueManagerApi.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CatalogueManagerApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FactoriesController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductsManager _productsManger;
        private readonly IFactoriesManager _factoriesManager;

        public FactoriesController(IMapper mapper, IProductsManager productsManger, IFactoriesManager factoriesManager)
        {
            _mapper = mapper;
            _productsManger = productsManger;
            _factoriesManager = factoriesManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllFactoriesAsnyc()
        {
            var factories = await _factoriesManager.GetAllFactoriesAsync();

            return Ok(_mapper.Map<List<FactoryResponseModel>>(factories));
        }

        [HttpPost]
        [Route("addOrder")]
        public async Task<IActionResult> AddFactoryAsync(FactoryRequestModel model)
        {
            var allProducts = await _productsManger.GetAllProductsAsync();
            if (model.Capabilities.Any(i => allProducts.All(p => p.Id != i.ProductId)))
            {
                return BadRequest("Some of the products within the factory capabilities does not exist");
            }

            await _factoriesManager.AddFactoryAsync(_mapper.Map<Factory>(model));

            return Ok();
        }

        [HttpGet]
        [Route("capabilities")]
        public async Task<IActionResult> GetAllCapabilitiesAsync()
        {
            var allOrderProducts = await _factoriesManager.GetAllFactoryProductsAsync();

            return Ok(_mapper.Map<List<FactoryProductWithFactoryDetailsResponseModel>>(allOrderProducts));
        }
    }
}