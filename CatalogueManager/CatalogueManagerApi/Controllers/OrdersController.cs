﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BL.Managers.Interfaces;
using CatalogueManagerApi.RequestModels;
using CatalogueManagerApi.ResponseModels;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace CatalogueManagerApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IProductsManager _productsManger;
        private readonly IOrdersManager _ordersManager;

        public OrdersController(IMapper mapper, IProductsManager productsManger, IOrdersManager ordersManager)
        {
            _mapper = mapper;
            _productsManger = productsManger;
            _ordersManager = ordersManager;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllOrdersAsnyc()
        {
            var orders = await _ordersManager.GetAllOrdersAsync();

            return Ok(_mapper.Map<List<OrderResponseModel>>(orders));
        }

        [HttpPost]
        [Route("addOrder")]
        public async Task<IActionResult> AddOrderAsync(OrderRequestModel model)
        {
            var allProducts = await _productsManger.GetAllProductsAsync();
            if (model.Items.Any(i => allProducts.All(p => p.Id != i.ProductId)))
            {
                return BadRequest("Some of the products within the order does not exist");
            }

            if (model.Deadline.Date < DateTime.Now.Date)
            {
                return BadRequest("The deadline should be in the future");
            }

            await _ordersManager.AddOrderAsync(_mapper.Map<Order>(model));

            return Ok();
        }

        [HttpGet]
        [Route("items")]
        public async Task<IActionResult> GetAllItemsAsync()
        {
            var allOrderProducts = await _ordersManager.GetAllOrderProductsAsync();

            return Ok(_mapper.Map<List<OrderProductWithOrderDetailsResponseModel>>(allOrderProducts));
        }
    }
}