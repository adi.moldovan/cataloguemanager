﻿using AutoMapper;
using CatalogueManagerApi.RequestModels;
using CatalogueManagerApi.ResponseModels;
using Models;
using System.Linq;

namespace CatalogueManagerApi.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ProductRequestModel, Product>();
            CreateMap<Product, ProductResponseModel>();
            CreateMap<OrderProductRequestModel, OrderProduct>();
            CreateMap<OrderRequestModel, Order>()
                .ForMember(dest => dest.Products, opt => opt.MapFrom(src => src.Items));
            CreateMap<OrderProduct, OrderProductResponseModel>();
            CreateMap<Order, OrderResponseModel>()
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src.Products));
            CreateMap<Order, OrderSummaryResponseModel>();
            CreateMap<OrderProduct, OrderProductWithOrderDetailsResponseModel>();
            CreateMap<FactoryProductRequestModel, FactoryProduct>();
            CreateMap<FactoryRequestModel, Factory>()
                .ForMember(dest => dest.Products, opt => opt.MapFrom(src => src.Capabilities));
            CreateMap<FactoryProduct, FactoryProductResponseModel>();
            CreateMap<Factory, FactoryResponseModel>()
                .ForMember(dest => dest.Capabilities, opt => opt.MapFrom(src => src.Products));
            CreateMap<Factory, FactorySummaryResponseModel>();
            CreateMap<FactoryProduct, FactoryProductWithFactoryDetailsResponseModel>();
            CreateMap<Order, OrderReportResponseModel>()
                .ForMember(dest => dest.Items, opt => opt.MapFrom(src => src.Products))
                .ForMember(dest => dest.OveralCost, opt => opt.MapFrom(src => src.Products.Sum(p => p.Product.Factories.Min(f => f.Cost) * p.Quantity)));
        }
    }
}