﻿using DataAccessLayer.Repositories.Interfaces;

namespace BL.UnitOfWorks
{
    public interface IUnitOfWork
    {
        IProductsRepository ProductsRepository { get; set; }

        IOrdersRepository OrdersRepository { get; set; }

        IOrderProductsRepository OrderProductRepository { get; set; }

        IFactoriesRepository FactoriesRepository { get; set; }

        IFactoryProductsRepository FactoryProductsRepository { get; set; }
    }
}