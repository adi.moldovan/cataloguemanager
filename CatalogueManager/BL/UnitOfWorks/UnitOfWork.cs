﻿using DataAccessLayer.Repositories.Interfaces;

namespace BL.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {
        public IProductsRepository ProductsRepository { get; set; }
        public IOrdersRepository OrdersRepository { get; set; }
        public IOrderProductsRepository OrderProductRepository { get; set; }
        public IFactoriesRepository FactoriesRepository { get; set; }
        public IFactoryProductsRepository FactoryProductsRepository { get; set; }

        public UnitOfWork(IProductsRepository productsRepository, IOrdersRepository ordersRepository, IOrderProductsRepository orderProductRepository,
            IFactoriesRepository factoriesRepository, IFactoryProductsRepository factoryProductsRepository)
        {
            ProductsRepository = productsRepository;
            OrdersRepository = ordersRepository;
            OrderProductRepository = orderProductRepository;
            FactoriesRepository = factoriesRepository;
            FactoryProductsRepository = factoryProductsRepository;
        }
    }
}