﻿using BL.Managers.Interfaces;
using BL.UnitOfWorks;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Managers
{
    public class OrdersManager : IOrdersManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public OrdersManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task AddOrderAsync(Order order)
        {
            return _unitOfWork.OrdersRepository.AddOrderAsync(order);
        }

        public Task<List<Order>> GetAllOrdersAsync()
        {
            return _unitOfWork.OrdersRepository.GetAllOrdersAsnyc();
        }

        public Task<List<Order>> GetAllActiveOrdersAsync()
        {
            return _unitOfWork.OrdersRepository.GetAllActiveOrdersAsnyc();
        }

        public Task<List<OrderProduct>> GetAllOrderProductsAsync()
        {
            return _unitOfWork.OrderProductRepository.GetAllOrderProducts();
        }
    }
}