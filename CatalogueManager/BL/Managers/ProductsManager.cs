﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BL.Managers.Interfaces;
using BL.UnitOfWorks;
using Models;

namespace BL.Managers
{
    public class ProductsManager : IProductsManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public ProductsManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task AddProductAsync(Product product)
        {
            return _unitOfWork.ProductsRepository.AddProductAsync(product);
        }

        public Task<List<Product>> GetAllProductsAsync()
        {
            return _unitOfWork.ProductsRepository.GetAllProductsAsync();
        }
    }
}