﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace BL.Managers.Interfaces
{
    public interface IProductsManager
    {
        Task AddProductAsync(Product product);

        Task<List<Product>> GetAllProductsAsync();
    }
}