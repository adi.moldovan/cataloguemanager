﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Managers.Interfaces
{
    public interface IOrdersManager
    {
        public Task AddOrderAsync(Order order);

        Task<List<Order>> GetAllOrdersAsync();

        Task<List<Order>> GetAllActiveOrdersAsync();

        Task<List<OrderProduct>> GetAllOrderProductsAsync();
    }
}