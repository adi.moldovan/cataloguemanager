﻿using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Managers.Interfaces
{
    public interface IFactoriesManager
    {
        public Task AddFactoryAsync(Factory factory);

        Task<List<Factory>> GetAllFactoriesAsync();

        Task<List<FactoryProduct>> GetAllFactoryProductsAsync();
    }
}