﻿using BL.Managers.Interfaces;
using BL.UnitOfWorks;
using Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BL.Managers
{
    public class FactoriesManager : IFactoriesManager
    {
        private readonly IUnitOfWork _unitOfWork;

        public FactoriesManager(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Task AddFactoryAsync(Factory factory)
        {
            return _unitOfWork.FactoriesRepository.AddFactoryAsync(factory);
        }

        public Task<List<Factory>> GetAllFactoriesAsync()
        {
            return _unitOfWork.FactoriesRepository.GetAllFactoriesAsnyc();
        }

        public Task<List<FactoryProduct>> GetAllFactoryProductsAsync()
        {
            return _unitOfWork.FactoryProductsRepository.GetAllFactoryProducts();
        }
    }
}